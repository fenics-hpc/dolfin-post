.TH "DOLFIN_POST" 1

.SH NAME
dolfin_post - DOLFIN HPC post processor

.SH SYNOPSIS
.B dolfin_post
[-s sample name]
[-n num samples]
[-m mesh name]
[-M multi mesh name]
[-t format]
[-o offset ]
[-f frequency]

.SH DESCRIPTION

DOLFIN post can be used to convert binary files written by DOLFIN HPC
to either VTK (paraview), silo (visit) dolfin (serialized version, for
reading into saaz etc).

.SH OPTIONS
The utility has the following arguments,
.TP
\fB\-s sample name\fR
The sample's filename (without number or suffix)
.TP
\fB\-n number of samples\fR, 
Number of files to convert. Starting from file 0, unless an offset is given (see -o option)
.TP
\fB\-m mesh name\fR, 
Name of mesh file (whole filename)
.TP
\fB\-M multi mesh name\fR,
Name of mesh file (without the number or suffix) 
.TP
\fB\-t output format\fR, 
Output format, either vtk, silo or dolfin
.TP
\fB\-o offset\fR 
Starting offset (without leading zeros)
.TP
\fB\-f\fR frequency
Conversion frequency
.TP
At a minimum -s, -n -t and either -M or -m has to be given.
.SH EXAMPLES
Convert 101 files start from binary002900.bin into dolfin:
.PP
.nf
.RS
dolfin_post -s binary -m mesh.bin -t dolfin -n 101 -o 2900
.RE
.fi
.PP
Convert binary000000.bin, binary000002.bin, binary0000004.bin, binary000006.bin, binary000008.bin into silo:
.PP
.nf
.RS
dolfin_post -s binary -m mesh.bin -t silo -n 10 -f 2
.RE
.fi
.PP
Convert ALE simulation into vtk, using data from, binary000000.bin - binary000002.bin and meshes, mesh000000.bin - mesh000002.bin:
.PP
.nf
.RS
dolfin_post -s primal_solution -M mesh -t vtk -n 2
.RE
.fi
.PP

.SH AUTHOR
Written by Niclas Jansson (njansson@kth.se)
