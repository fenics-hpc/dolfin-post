/*
Copyright (c) 2011, Niclas Jansson <njansson@csc.kth.se>
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of the author nor the names of its contributors may be
      used to endorse or promote products derived from this software without
      specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/


#include <dolfin_binary.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <dolfin_export.h>
#include <dolfin_post.h>
#include <silo_export.h> 
#include <vtk_export.h>
#include <util.h>


void usage() {
  fprintf(stderr, "Usage: "
	  "-s <sample name> "
	  "-n <number of samples> "
	  "[-m <mesh name> -M <multi mesh name>] "
	  "-t <output format> "
	  "[-o <offset>] "
	  "[-f <frequency>] "
	  "[-b <byte swap>]\n");
}

int read_mesh(char *mesh_fname, Mesh *mesh, uint8_t bswap) {
  BinaryFileHeader mesh_hdr;
  FILE *mesh_fp;  
  int *tmp_cells;
  double *tmp_vertices;

  if ( (mesh_fp = fopen(mesh_fname, "r")) == 0) {
    perror(mesh_fname);
    return -1;
  } 

  fread(&mesh_hdr, sizeof(BinaryFileHeader), 1, mesh_fp);    
  if (bswap) 
    bswap_hdr(&mesh_hdr);
  if ((mesh_hdr.magic != BINARY_MAGIC_V1 &&
       mesh_hdr.magic != BINARY_MAGIC_V2) ||
      mesh_hdr.type != BINARY_MESH_DATA)
    return -1;
  

  fread(&(mesh->dim), sizeof(int), 1, mesh_fp);
  fread(&(mesh->type), sizeof(int), 1, mesh_fp);
  fread(&(mesh->nvertices), sizeof(int), 1, mesh_fp);

  if(bswap) {
    mesh->dim = bswap_int(mesh->dim);
    mesh->type = bswap_int(mesh->type);
    mesh->nvertices = bswap_int(mesh->nvertices); 
  }

  if (mesh_hdr.magic == BINARY_MAGIC_V1)
    mesh->type = (mesh->type + 3);
  else
    mesh->type = (mesh->type + 1);

  if (mesh->vertices) {
    tmp_vertices = mesh->vertices;
    mesh->vertices = realloc(tmp_vertices,
			     mesh->dim * mesh->nvertices * sizeof(double)); 
  }
  else    
    mesh->vertices = malloc(mesh->dim * mesh->nvertices * sizeof(double));  
  fread(mesh->vertices, sizeof(double), mesh->dim * mesh->nvertices , mesh_fp);

  fread(&mesh->ncells, sizeof(int), 1, mesh_fp);  

  if (bswap) {
    bswap_data(mesh->vertices, mesh->dim * mesh->nvertices);    
    mesh->ncells = bswap_int(mesh->ncells);
  }
  
  if (mesh->cells) {
    tmp_cells = mesh->cells;
    mesh->cells = realloc(tmp_cells, 
			  mesh->ncells * mesh->type * sizeof(int));
  }
  else
    mesh->cells = malloc(mesh->ncells * mesh->type * sizeof(int));
  fread(mesh->cells, sizeof(int), mesh->ncells * mesh->type, mesh_fp);
  fclose(mesh_fp);

  if (bswap)
    bswap_iarr(mesh->cells, mesh->ncells * mesh->type);

  return 0;
}

void update_fname(int j, uint8_t multimesh, int format,
		  char *data_fname, char *sample_fname, char *mesh_fname, 
		  char *base_fname, char *mesh_base) {
  
  char suffix[16], data_suffix[16];
  
  sprintf(suffix, "%.06d.bin", j);
  
  if (format == VTK)
    sprintf(data_suffix, "%.06d.vtu", j);
  else if(format == SILO)
    sprintf(data_suffix, "%.06d.silo", j);
  else if(format == DOLFIN)
    sprintf(data_suffix, "S%.06d.bin", j);
  
  strcpy(sample_fname, base_fname);
  strcat(sample_fname, suffix);
  strcpy(data_fname, base_fname);
  strcat(data_fname, data_suffix);
  
  if (multimesh) {
    strcpy(mesh_fname, mesh_base);
    strcat(mesh_fname, suffix);
  }
}

int main(int argc, char **argv) {
  BinaryFileHeader hdr;
  BinaryFunctionHeader f_hdr;
  FILE *col_fp, *data_fp, *sam_fp;
  Mesh mesh;
  uint8_t multimesh, bswap;
  int num_samples, freq;
  int mfunc_type, num_func, i,j, arg, format, offset; 
  double *data;
  char base_fname[FNAME_LENGTH], sample_fname[FNAME_LENGTH], 
    data_fname[FNAME_LENGTH], col_fname[FNAME_LENGTH], 
    mesh_base[FNAME_LENGTH], mesh_fname[FNAME_LENGTH];
  
  
#ifdef HAVE_SILO
  DBfile *dbfile;
#else
  void *dbfile;
#endif

  offset = multimesh = num_samples = 0;
  freq = 1;
  multimesh = 0;
  bswap = 0;
  format = UNKNOWN;
  data = NULL;
  col_fp = data_fp = NULL;
  mesh.cells = NULL;
  mesh.vertices = NULL;
  dbfile = NULL;

  if (argc < 9) {
    usage(); 
    return -1;
  }

  while( -1 != (arg = getopt(argc, argv, "s:n:m:M:f:t:o:b"))) {
    switch(arg) {
    case 's':      
      strcpy(base_fname, optarg);
      break;
    case 'n':
      num_samples = atoi(optarg);
      break;
    case 'm':
      strcpy(mesh_fname, optarg);
      break;
    case 'M':
      multimesh = 1;
      strcpy(mesh_base, optarg);
      break;
    case 'o':
      offset = atoi(optarg);
      break;
    case 'f':
      freq = atoi(optarg);
      break;
    case 'b':
      bswap = 1;
      break;
    case 't':
      if(strcmp(optarg, "dolfin") == 0)
	format = DOLFIN;
#ifdef HAVE_GLIB
      else if (strcmp(optarg, "vtk") == 0)
	format = VTK;
#endif
#ifdef HAVE_SILO
      else if(strcmp(optarg, "silo") == 0)
	format = SILO;
#endif
      else {
	fprintf(stderr, "Unknown output format\n");
	return -1;
      }
      break;
    default:
      fprintf(stderr, "Unknown or missing argument\n");
      return -1;
      break;
    }
  }

  argc -= optind;
  if (argc != 0) {
    usage(); 
    return -1;
  }

  printf("This is %s, Version %s\n", PACKAGE_NAME, PACKAGE_VERSION);  

  strcpy(col_fname, base_fname);
  if (format == VTK)   
    strcat(col_fname, ".pvd");
  else if(format == SILO)
    strcat(col_fname, ".visit");  
  if (format != DOLFIN)
    if ( (col_fp = fopen(col_fname, "w")) == 0) {
      perror(col_fname);
      return -1;
    }

  if (format == VTK)
    write_pvdheader_open(col_fp);

  if (!multimesh) 
    if (read_mesh(mesh_fname, &mesh, bswap) != 0)
      return -1;
  
  
  for (j = offset; j < ((offset > 0) ? 
			(offset + num_samples) : num_samples); j += freq) {
    
    update_fname(j, multimesh, format, 
		 data_fname, sample_fname, mesh_fname,
		 base_fname, mesh_base);
    
    if (multimesh) 
      if (read_mesh(mesh_fname, &mesh, bswap) != 0)
	return -1;
    
    if ( (sam_fp = fopen(sample_fname, "r")) == 0) {
      if (format == VTK) 
	write_pvdheader_close(col_fp);
      perror(sample_fname);
      return -1;
    }
    else if (format == SILO) {
#ifdef HAVE_SILO
#ifdef HAVE_SILO_HDF5
      DBSetCompression("METHOD=GZIP ERRMODE=FALLBACK LEVEL=9");
      dbfile = DBCreate(data_fname, DB_CLOBBER, DB_LOCAL, NULL, DB_HDF5);
#else
      dbfile = DBCreate(data_fname, DB_CLOBBER, DB_LOCAL, NULL, DB_PDB);
#endif
#endif
    }

    printf("%s\n", sample_fname);

    fread(&hdr, sizeof(BinaryFileHeader), 1, sam_fp);
    if (bswap) 
      bswap_hdr(&hdr);
    
    if (hdr.magic != BINARY_MAGIC_V1 && hdr.magic != BINARY_MAGIC_V2)
      return -1;
    
    if (format == VTK || format == DOLFIN) {
      if ( (data_fp = fopen(data_fname, "w")) == 0) {
	perror(data_fname);
	return -1;
      }
      
      if (format == VTK) {
	write_vtkheader_open(data_fp, mesh.ncells, mesh.nvertices);
	write_vtkpoints(data_fp, mesh.vertices, mesh.dim, mesh.nvertices);
	write_vtkcells(data_fp, mesh.cells, mesh.type, mesh.ncells, mesh.dim);
      }
      else if (format == DOLFIN) {
	write_dolfinheader(data_fp, hdr);
      }
    }
  
    if (hdr.type == BINARY_FUNCTION_DATA) {
      fread(&num_func, sizeof(int), 1, sam_fp);
      if (bswap) 
	num_func = bswap_int(num_func);
      if (format == VTK)
	write_vtkpointdata_open(data_fp);
      else if (format == DOLFIN)
	write_dolfinnumfunc(data_fp, num_func);

      for (i = 0; i < num_func; i++) {
	fread(&f_hdr, sizeof(BinaryFunctionHeader), 1, sam_fp);
	if (bswap)
	  bswap_fhdr(&f_hdr);
       if (data == NULL)
	 data = malloc(f_hdr.size * sizeof(double));
       else 
	 data = realloc(data, f_hdr.size * sizeof(double));
        fread(data, sizeof(double), f_hdr.size, sam_fp);    
	if (bswap)
	  bswap_data(data, f_hdr.size);

	if (format == VTK)
	  write_vtkpointdata(data_fp, data, f_hdr);
	else if (format == SILO) {
	  if (i == 0)
	    write_silomesh(dbfile, &mesh, &f_hdr.t);

	  write_silopointdata(dbfile, data, f_hdr);
	}
	else if (format == DOLFIN) 
	  write_dolfinpointdata(data_fp, data, f_hdr);
	else {
	  fprintf(stderr, "Output not implemented!\n");
	  return 1;
	}

      }
      if (format == VTK)
	write_vtkpointdata_close(data_fp);
    }
    else if (hdr.type == BINARY_MESH_FUNCTION_DATA) {
      fread(&mfunc_type, sizeof(int), 1, sam_fp);
      if (bswap)
	mfunc_type = bswap_int(mfunc_type);

      if(format == VTK) 
	(mfunc_type > 0 ? write_vtkpointdata_open(data_fp) : 
	 write_vtkcelldata_open(data_fp));
      else {
	fprintf(stderr, "Output not implemented!\n");
	return 1;
      }

       if (data == NULL)
	 data = malloc((mfunc_type > 0 ? 
			mesh.nvertices : mesh.ncells) * sizeof(double));
       else 
	 data = realloc(data, (mfunc_type > 0 ? 
			       mesh.nvertices : mesh.ncells) * sizeof(double));
       fread(data, sizeof(double), (mfunc_type > 0 ? 
				    mesh.nvertices : mesh.ncells), sam_fp);    
       if (bswap) 
	 bswap_data(data, (mfunc_type > 0 ? mesh.nvertices : mesh.ncells));
       if (format == VTK) {
	 write_vtkarray(data_fp, data, (mfunc_type > 0 ? 
					mesh.nvertices : mesh.ncells));		
	 (mfunc_type > 0 ? write_vtkpointdata_close(data_fp) : 
	  write_vtkcelldata_close(data_fp));     
       }       
    }
    
    
    fclose(sam_fp);
    
    
    if (format == VTK || format == DOLFIN) {
      if (format == VTK) {
	write_vtkheader_close(data_fp);
	write_pvddataset(col_fp, data_fname, 
			 (hdr.type == BINARY_FUNCTION_DATA  ? f_hdr.t : j));
      }
      fclose(data_fp);
    }
#ifdef HAVE_SILO
    else if(format == SILO) {
      DBClose(dbfile);    
      fprintf(col_fp, "%s\n", data_fname);
    }
#endif
  }
  if (format == VTK)
    write_pvdheader_close(col_fp);
  
  if (format != DOLFIN)
    fclose(col_fp);

  if (mesh.cells)
    free(mesh.cells);
  if (mesh.vertices)
    free(mesh.vertices);  
  if (data)
    free(data);

  return 0;
}


