#ifndef __DOLFIN_POST_H
#define __DOLFIN_POST_H

enum {VTK, SILO, DOLFIN, UNKNOWN};

typedef struct {
  double *vertices;
  int *cells;
  int ncells;
  int nvertices;
  int type;
  int dim;
} Mesh;

#endif
