#ifndef __DOLFIN_BINARY_H
#define __DOLFIN_BINARY_H

#include <stdint.h>

#define BINARY_MAGIC_V1 0xBABE
#define BINARY_MAGIC_V2 0xB4B3
#define FNAME_LENGTH 256

enum { BINARY_MESH_DATA, 
       BINARY_VECTOR_DATA,
       BINARY_FUNCTION_DATA,
       BINARY_MESH_FUNCTION_DATA};

typedef struct {
  uint32_t magic;
  uint32_t bendian; 
  uint32_t pe_size;
  uint32_t type;
} BinaryFileHeader;

typedef struct {
  uint32_t dim;
  uint32_t size;
  double t;
  char name[FNAME_LENGTH];      
} BinaryFunctionHeader;

#endif
