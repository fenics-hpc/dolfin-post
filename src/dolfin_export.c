/*
Copyright (c) 2012, Niclas Jansson <njansson@csc.kth.se>
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation 
      and/or other materials provided with the distribution.
    * Neither the name of the author nor the names of its contributors may be 
      used to endorse or promote products derived from this software without 
      specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include <stdlib.h>
#include <dolfin_export.h>

void write_dolfinnumfunc(FILE *fp, int num_func){ 
  fwrite(&num_func, sizeof(int), 1, fp);
}

void write_dolfinheader(FILE *fp, BinaryFileHeader hdr) {  
  hdr.pe_size = 1;
  fwrite(&hdr, sizeof(BinaryFileHeader), 1, fp);
}

void write_dolfinpointdata(FILE *fp, double *data, BinaryFunctionHeader hdr) {
  
  int i, j, k, comp_size;
  double *tmp;
  
  fwrite(&hdr, sizeof(BinaryFunctionHeader), 1, fp);
  if (hdr.dim > 1) {
    comp_size = hdr.size / hdr.dim;
    tmp = malloc(comp_size * sizeof(double));
    
    for (k = 0; k < hdr.dim; k++ ) {
      for (i = k, j = 0; i < hdr.size; j++, i += hdr.dim) 
	tmp[j] = data[i];
      fwrite(tmp,sizeof(double), comp_size, fp);
    }
    free(tmp);
  }
  else 
    fwrite(data, sizeof(double), hdr.size, fp);
}
